# VirtualTest

To send Load-curve from Swift program (Filename: SwiftServer) to Labview (Filename: Client-Receives Load Curve.vi), follow the below steps,

Step 1 : Get the Load curve:
Get the path of the load curve you need to pass it to the labview. Enter this path inside the main.swift of the SwiftServer folder, and save it.

Step 2 : Start the Server:
Swift program is the server that sends the Load curve to the Labview. 
cd into the folder VirtualTest, and use below command to start the Swift server
swift run SwiftServer

Step 3 : Run the Client: 
Labview is the client. Have the file Client-Receives Load Curve.vi with you. It can be found in: Gitlab->PassiveLogic->testandvalidation->testcenter->Development->Meghna Labview->Client-Receives Load Curve.vi
The .vi has to be run on a windows machine.
Open the vi and use the run button to run it.

Step 4 : Output
After the timeout period of 20s (as per the current value set in the .vi), you will notice the load curve values updated in the front panel of the .vi, inside the configuration array

