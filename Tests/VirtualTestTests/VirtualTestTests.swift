import XCTest
@testable import VirtualTest

final class VirtualTestTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(VirtualTest().text, "Hello, World!")
    }
}
