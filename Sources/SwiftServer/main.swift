
// A server that sends load curve to Labview

import NIOCore
import NIOPosix
import Foundation



// Processes data exchanged between the server and client
private final class CSVHandler: ChannelInboundHandler {
    public typealias InboundIn = ByteBuffer
    public typealias OutboundOut = ByteBuffer
    private var sendBytes = 0
    
    public func channelActive(context: ChannelHandlerContext){
        let channel = context.channel
        let csvData = readCSVFile()
       
        
        // Buffer holds the data that has to be written into the client.
        let buffer = channel.allocator.buffer(string: csvData)
        self.sendBytes = buffer.readableBytes
        context.writeAndFlush(self.wrapOutboundOut(buffer), promise: nil)
        
    }
    
    // Flush it out. This can make use of gathering writes if multiple buffers are pending
    public func channelReadComplete(context: ChannelHandlerContext) {
        context.flush()
    }
    
    public func errorCaught(context: ChannelHandlerContext, error: Error) {
       print("error: ", error)

       // As we are not really interested getting notified on success or failure we just pass nil as promise to
       // reduce allocations.
       context.close(promise: nil)
    }
    
    
}

// Reads Load curve CSV file from the path provided. Data passed to the TCP protocol has to be a string.
public func readCSVFile() -> String {
    do {
        let csvPath = "/Users/meghnamanjunath/Meghna-Dir/VirtualTest/Sources/Load Input-Load Curve.csv"
        let csvfileURL = URL(fileURLWithPath: csvPath)
        let csvData = try String(contentsOf: csvfileURL)
        
        return csvData
    }
    catch {
        return ""
    }
}

let group = MultiThreadedEventLoopGroup(numberOfThreads: System.coreCount)
// Sets up Server side configuration
let bootstrap = ServerBootstrap(group: group)
    // Specify backlog and enable SO_REUSEADDR for the server itself
    .serverChannelOption(ChannelOptions.backlog, value: 256)
    .serverChannelOption(ChannelOptions.socketOption(.so_reuseaddr), value: 1)

    // Set the handlers that are appled to the accepted Channels
    .childChannelInitializer { channel in
        // Ensure we don't read faster than we can write by adding the BackPressureHandler into the pipeline.
        channel.pipeline.addHandler(BackPressureHandler()).flatMap { v in
            channel.pipeline.addHandler(CSVHandler())
        }
    }

    // Enable SO_REUSEADDR for the accepted Channels
    .childChannelOption(ChannelOptions.socketOption(.so_reuseaddr), value: 1)
    .childChannelOption(ChannelOptions.maxMessagesPerRead, value: 16)
    .childChannelOption(ChannelOptions.recvAllocator, value: AdaptiveRecvByteBufferAllocator())
defer {
    try! group.syncShutdownGracefully()
}

// First argument is the program path
let arguments = CommandLine.arguments
let arg1 = arguments.dropFirst().first
let arg2 = arguments.dropFirst(2).first

let defaultHost = "10.20.31.69" // IP Address of my mac
let defaultPort = 8089

// Creating server socket and binding it to IP address and Port
enum BindTo {
    case ip(host: String, port: Int)
    case unixDomainSocket(path: String)
}

let bindTarget: BindTo
switch (arg1, arg1.flatMap(Int.init), arg2.flatMap(Int.init)) {
case (.some(let h), _ , .some(let p)):
    /* we got two arguments, let's interpret that as host and port */
    bindTarget = .ip(host: h, port: p)
case (.some(let portString), .none, _):
    /* couldn't parse as number, expecting unix domain socket path */
    bindTarget = .unixDomainSocket(path: portString)
case (_, .some(let p), _):
    /* only one argument --> port */
    bindTarget = .ip(host: defaultHost, port: p)
default:
    bindTarget = .ip(host: defaultHost, port: defaultPort)
}


let channel = try { () -> Channel in
    switch bindTarget {
    case .ip(let host, let port):
        return try bootstrap.bind(host: host, port: port).wait()  // After server is configured, it is binded to port and IP address
    case .unixDomainSocket(let path):
        return try bootstrap.bind(unixDomainSocketPath: path).wait()
    }
}()

guard let localAddress = channel.localAddress else {
    fatalError("Address was unable to bind. Please check that the socket was not closed or that the address family was understood.")
}

print("Server started and listening on \(channel.localAddress!)")

// This will never unblock as we don't close the ServerChannel
try channel.closeFuture.wait()


// Close the connection
print("Server closed")




